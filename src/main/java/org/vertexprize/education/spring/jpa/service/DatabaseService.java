/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.jpa.service;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vertexprize.education.spring.jpa.entity.Student;
import org.vertexprize.education.spring.jpa.repository.StudentRepository;

/**
 *
 * @author vaganovdv
 */
@Service
public class DatabaseService {

    private static final Logger log = LoggerFactory.getLogger(DatabaseService.class);
    
    @Autowired
    StudentRepository repository;

    /**
     * Подсчет количества студентов 
     * 
     * @return 
     */
    public long countStundens ()
    {
        return repository.count();        
    }    
    
   
    
    
    
    /**
     * Получение полного списка студентов 
     * @return 
     */
    public List<Student> getAllStundents() {
        
        List<Student> students = new ArrayList<>();
        
        // Поиск всех студентов в репозитории
        Iterable<Student> allStudnets = repository.findAll();
        
        // Добавление студентов в список
        allStudnets.forEach( s -> students.add(s));
        
        return students;
    }

    /**
     * Сохранение студента
     * 
     * @param student 
     */
    public void saveStudent(Student student) {
        repository.save(student);                
    }

    
    /**
     * Поиск студентов по фамилиии 
     * 
     * @param sureName
     * @return 
     */
    public List<Student> findStudentBySureName (String sureName)
    {
        return repository.findBySureName(sureName);
    }   
    
    /**
     * Поиск студентов по отчеству 
     * 
     * @param middleName
     * @return 
     */
    public List<Student> findStudentByMiddleName (String middleName)
    {         
        return repository.findByMiddleName(middleName);
    }  
    
    
}
