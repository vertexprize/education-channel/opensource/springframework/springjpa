/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.jpa.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *  Класс Student при использовании JPA
 * 
 * @author vaganovdv
 */
@Entity

@Setter
@Getter
@NoArgsConstructor
public class Student {
    
    // Вариант 1 
    // Генерация строкового представления идентификатора id
    //@Id
    //@GeneratedValue(strategy = GenerationType.UUID)
    // пример 123e4567-e89b-12d3-a456-426655440000
    //private String id;
   
    // Вариант 2
    // Генерация числоового представления идентификатора id
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id; // Идентификатор записи студента 
    
    // Поля класса Student
    //
    private String firstName;
    private String sureName;
    private String middleName;
    private String studentGroup;
    private int  year;
    
}
