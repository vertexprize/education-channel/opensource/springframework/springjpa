package org.vertexprize.education.spring.jpa;

import jakarta.annotation.PostConstruct;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.vertexprize.education.spring.jpa.entity.Student;
import org.vertexprize.education.spring.jpa.service.DatabaseService;

@SpringBootApplication
public class SpringJpaApplication {

    private static final Logger log = LoggerFactory.getLogger(SpringJpaApplication.class);
    
    private final DatabaseService databaseService;

    public SpringJpaApplication(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }
    
    
    
	public static void main(String[] args) {
		SpringApplication.run(SpringJpaApplication.class, args);
	}

        
        
        
        
    @PostConstruct
    public void start() {
        log.info("Подсчет количества студентов в базе данных ...");
        databaseService.countStundens();

        log.info("Получение полного списка студентов в базе  ...");
        List<Student> stundents = databaseService.getAllStundents();

        if (!stundents.isEmpty()) {
            stundents.stream().forEach(stundent -> {
                log.info(stundent.toString());
            });
        }

        log.info("Добавление студентов ... ");

        Student student1 = new Student();
        student1.setFirstName("Дмитрий");
        student1.setMiddleName("Романович");
        student1.setSureName("Татарчук");
        student1.setStudentGroup("ПГС-21");
        student1.setYear(5);
        
        
        Student student2 = new Student();
        student2.setFirstName("Иван");
        student2.setMiddleName("Иванович");
        student2.setSureName("Иванович");
        student2.setStudentGroup("ПГС-21");
        student2.setYear(5);

        
        Student student3 = new Student();
        student3.setFirstName("Кирилл");
        student3.setMiddleName("Романович");
        student3.setSureName("Кичигин");
        student3.setStudentGroup("ПГС-21");
        student3.setYear(5);
        
        // Добавление студентов в базу
        databaseService.saveStudent(student1);
        databaseService.saveStudent(student2);
        databaseService.saveStudent(student3);
        

        log.info("Получение полного списка студентов в базе  ...");
        stundents = databaseService.getAllStundents();

        if (!stundents.isEmpty()) {
            stundents.stream().forEach(stundent -> {
                log.info(stundent.toString());
            });
        }
        
        log.info("Поиск студента с фамилией Татарчук ...");
        
        List<Student> students = databaseService.findStudentBySureName("Татарчук");
        log.info("Найдено ["+students.size()+"] записей студетов с фамилией [Татарчук]");
        
        log.info("Поиск студентов с отчетвом  Романович ...");
        
        students = databaseService.findStudentByMiddleName("Романович");
        log.info("Найдено ["+students.size()+"] записей студетов с фамилией [Романович]");

    }
}
