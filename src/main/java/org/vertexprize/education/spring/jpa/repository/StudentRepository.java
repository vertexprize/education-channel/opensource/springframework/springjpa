/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.jpa.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.vertexprize.education.spring.jpa.entity.Student;

/**
 *
 * @author vaganovdv
 */
public interface StudentRepository extends CrudRepository<Student, Long> {

//    /**
//     * Поиск студента по фамилии 
//     * @param sureName
//     * @return 
//     */
       List<Student> findBySureName(String sureName);    
//    
//    /**
//     * Поиск студентов по отчетству
//     * @param middleName
//     * @return 
//     */
       List<Student> findByMiddleName(String middleName);    
    
}
